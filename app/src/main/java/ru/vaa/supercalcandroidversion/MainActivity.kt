package ru.vaa.supercalcandroidversion

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val ANSWER = "answer"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun answer(view: View) {
        val oneNumber = etOneNumber.text.toString().toDouble()
        val twoNumber = etTwoNumber.text.toString().toDouble()

        val answer = when (view.id) {
            R.id.btnPlus -> oneNumber + twoNumber
            R.id.btnMinus -> oneNumber - twoNumber
            R.id.btnMultiple -> oneNumber * twoNumber
            R.id.btnDivide -> oneNumber / twoNumber
            else -> null
        }
        val intent = Intent(this, AnswerActivity::class.java)
        intent.putExtra(ANSWER, answer.toString())
        startActivity(intent)
    }
}