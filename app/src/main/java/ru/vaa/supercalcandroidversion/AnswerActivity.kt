package ru.vaa.supercalcandroidversion

import android.app.Activity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_answer.*

class AnswerActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_answer)
        tvAnswer.text = intent?.getStringExtra(MainActivity.ANSWER).toString()
    }
}
